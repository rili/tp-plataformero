using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaControlable : MonoBehaviour
{
    public int velocidad;
    //private Rigidbody rb;
    private bool subir = false;
    private bool bajar = false;


    // Start is called before the first frame update

    void Start()
    {

    }

    private void OnEnable()
    {
        if (GiraDireccion.lado == true)
        {
            transform.Translate(velocidad * Vector3.right * Time.deltaTime);
        }
        else if (GiraDireccion.lado == false)
        {
            transform.Translate(velocidad * Vector3.left * Time.deltaTime);
        }
        if (subir == true)
        {
            transform.Translate(velocidad * Vector3.up * Time.deltaTime);
        }
        if (bajar == true)
        {
            transform.Translate(velocidad * Vector3.down * Time.deltaTime);
        }

    }


    private void OnTriggerEnter(Collider other)
    {
        //if (other.gameObject.CompareTag("Enemy") == true)
        //{
        //    Vector3 enemyPos = other.transform.position;
        //    Vector3 playerPos = GameObject.FindGameObjectWithTag("Player7").transform.position;

        //    other.transform.position = playerPos;
        //    GameObject.FindGameObjectWithTag("Player7").transform.position = enemyPos;
        //    gameObject.SetActive(false);
        //}

        if (other.gameObject.CompareTag("Limit") == true)
        {
            Vector3 enemyPos = other.transform.position;
            Vector3 playerPos = GameObject.FindGameObjectWithTag("Player7").transform.position;

            other.transform.position = playerPos;
            GameObject.FindGameObjectWithTag("Player7").transform.position = enemyPos;
            gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("Player") == true)
        {
            Vector3 enemyPos = other.transform.position;
            Vector3 playerPos = GameObject.FindGameObjectWithTag("Player7").transform.position;

            other.transform.position = playerPos;
            GameObject.FindGameObjectWithTag("Player7").transform.position = enemyPos;
            gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("Water") == true)
        {
            Vector3 enemyPos = other.transform.position;
            Vector3 playerPos = GameObject.FindGameObjectWithTag("Player7").transform.position;

            other.transform.position = playerPos;
            GameObject.FindGameObjectWithTag("Player7").transform.position = enemyPos;
            gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("Target") == true)
        {
            Vector3 enemyPos = other.transform.position;
            Vector3 playerPos = GameObject.FindGameObjectWithTag("Player7").transform.position;

            other.transform.position = playerPos;
            GameObject.FindGameObjectWithTag("Player7").transform.position = enemyPos;
            gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("Trampoline") == true)
        {
            Vector3 enemyPos = other.transform.position;
            Vector3 playerPos = GameObject.FindGameObjectWithTag("Player7").transform.position;

            other.transform.position = playerPos;
            GameObject.FindGameObjectWithTag("Player7").transform.position = enemyPos;
            gameObject.SetActive(false);
        }
        //if (other.tag == "Enemy" || other.tag == "Target")
        //{
        //    Vector3 enemyPos = other.transform.position;
        //    Vector3 playerPos = GameObject.FindGameObjectWithTag("Player7").transform.position;

        //    other.transform.position = playerPos;
        //    GameObject.FindGameObjectWithTag("Player7").transform.position = enemyPos;
        //}




    }
    // Update is called once per frame
    new void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            subir = true;
        }
        if (Input.GetKeyUp(KeyCode.W))
        {
            subir = false;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            bajar = true;
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            bajar = false;
        }
        if (gameObject.activeSelf)
        {
            OnEnable();
        }
    }
}
