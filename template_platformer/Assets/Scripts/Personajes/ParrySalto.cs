using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParrySalto : Controller_Player
{
    public GameObject cubePrefab;  // Prefab del cubo que se crear�
    public float creationDelay = 0.1f;  // Tiempo de espera antes de crear el cubo
    public float cubeLifetime = 0.1f;  // Duraci�n de vida del cubo
    public float pushForce = 100f;  // Fuerza con la que el jugador es empujado al chocar con el cubo
    public GameObject Jugador;

    private bool canCreateCube = true; // Variable para evitar crear cubos demasiado seguido

    //public Rigidbody rb;

    // Update is called once per frame
    new void  FixedUpdate()
    {
        if (GameManager.actualPlayer == 0)
        { 

        if (Input.GetKeyDown(KeyCode.Mouse0) && canCreateCube && GameManager.actualPlayer == playerNumber)
            {
                StartCoroutine(CreateCubeWithDelay());
            }
        }
    }

    // Corutina que crea un cubo despu�s de un tiempo de espera
    IEnumerator CreateCubeWithDelay()
    {
        canCreateCube = false;

        // Crea un nuevo cubo debajo del jugador
        // Vector3 spawnPosition = transform.position - Vector3.up;
        // GameObject cube = Instantiate(cubePrefab, spawnPosition, Quaternion.identity);
        cubePrefab.SetActive(true);

        yield return new WaitForSeconds(creationDelay);

        // Destruye el cubo creado
        cubePrefab.SetActive(false);

        canCreateCube = true;
    }

    // Si el jugador choca con el cubo, lo empuja hacia arriba
    //private void OnCollisionEnter(Collision other)
    //{
    //    if (other.gameObject.tag == "Target")
    //    {
    //        rb.AddForce(Vector3.up * pushForce, ForceMode.Impulse);
    //    }
    //}
}
