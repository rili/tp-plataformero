using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitcubo : MonoBehaviour
{
    public float pushForce = 10f;
    public Rigidbody rb;
    // Start is called before the first frame update

    new void FixedUpdate()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Target")
        {
            rb.AddForce(new Vector3(0,  pushForce, 0), ForceMode.Impulse);
        }
        if (other.gameObject.CompareTag("Trampoline") == true)
        {
            rb.AddForce(new Vector3(0, pushForce, 0), ForceMode.Impulse);
        }
        if (other.gameObject.CompareTag("Water") == true)
        {
            rb.AddForce(new Vector3(0,  pushForce, 0), ForceMode.Impulse);
        }
        if (other.gameObject.CompareTag("Floor") == true)
        {
            rb.AddForce(new Vector3(0,  pushForce, 0), ForceMode.Impulse);
        }
        if (other.gameObject.CompareTag("Enemy"))
        {
            rb.AddForce(new Vector3(0, pushForce, 0), ForceMode.Impulse);
        }
        if (other.gameObject.CompareTag("Player") == true)
        {
            rb.AddForce(new Vector3(0, pushForce, 0), ForceMode.Impulse);
        }
        if (other.gameObject.CompareTag("Player7") == true)
        {
            rb.AddForce(new Vector3(0, pushForce, 0), ForceMode.Impulse);
        }
    }

}
