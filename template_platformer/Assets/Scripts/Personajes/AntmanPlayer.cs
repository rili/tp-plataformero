using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntmanPlayer : Controller_Player
{
    [SerializeField] public float escalaMinima = 0.5f;
    [SerializeField] public float escalaMaxima = 2f;
    [SerializeField] public float velocidadCambioEscala;

    private bool escalaMasGrande = false;
    private bool escalaMasPequeña = false;
    private bool PuedeMoveLeft, PuedeMoveRight, PuedeJump;
    
    Vector3 nuevaEscala;

    public override bool IsOnSomething()
    {
        return Physics.BoxCast(transform.position, new Vector3(transform.localScale.x * 0.9f, transform.localScale.y / 3, transform.localScale.z * 0.9f), Vector3.down, out downHit, Quaternion.identity, downDistanceRay);
    }

    public new void FixedUpdate()
    {
        if (GameManager.actualPlayer == 4)
        {
            Habilidad();
        }
    }

    //Cambia la escala del jugador en la dirección dada por la variable "direccion"
    public void CambiarEscala(float direccion)
    {

        nuevaEscala = transform.localScale + new Vector3(direccion, direccion, direccion);
        nuevaEscala = Vector3.Max(nuevaEscala, new Vector3(escalaMinima, escalaMinima, escalaMinima));
        nuevaEscala = Vector3.Min(nuevaEscala, new Vector3(escalaMaxima, escalaMaxima, escalaMinima));
        transform.localScale = nuevaEscala;

    }

    public void Habilidad ()
    {
        //Tecla para hacer más grande
        if (Input.GetKey(KeyCode.Mouse0))
        {
            escalaMasGrande = true;
        }

        else
        {
            escalaMasGrande = false;
        }

        // Tecla para hacer más pequeño
        if (Input.GetKey(KeyCode.Mouse1))
        {
            escalaMasPequeña = true;
        }
        else
        {
            escalaMasPequeña = false;
        }

        // Cambia la escala del jugador
        if (escalaMasGrande)
        {
            CambiarEscala(velocidadCambioEscala);
        }
        else if (escalaMasPequeña)
        {
            CambiarEscala(-velocidadCambioEscala);
        }

    }
}







    