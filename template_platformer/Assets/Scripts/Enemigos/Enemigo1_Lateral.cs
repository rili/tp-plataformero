using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo1_Lateral : MonoBehaviour
{
    static public float velocidad = 2.0f;   // velocidad de movimiento
    public float distancia = 5.0f;  // distancia m�xima de movimiento

    private Vector3 direccion = Vector3.right;  // direcci�n inicial de movimiento
    private Vector3 origen;                      // posici�n inicial del enemigo

    void Start()
    {
        origen = transform.position;
    }

    void Update()
    {
        // Mover el enemigo en la direcci�n actual
        transform.Translate(direccion * velocidad * Time.deltaTime);

        // Cambiar la direcci�n si el enemigo ha alcanzado su l�mite de movimiento
        if (Vector3.Distance(transform.position, origen) > distancia)
        {
            direccion *= -1;
        }
    }
}
