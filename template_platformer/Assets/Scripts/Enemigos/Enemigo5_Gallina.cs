using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo5_Gallina : MonoBehaviour
{

    public  Transform door;
    public  Transform openTransform;
    public  Transform closedTransform;
    //public static bool desbloqueada;
    public  float doorSpeed = 1f;
    Vector3 targetPosition;
     float time;

    // Start is called before the first frame update
    void Start()
    {
        targetPosition = closedTransform.position;
    }

    // Update is called once per frame
   void FixedUpdate()
    {
        if ( door.position != targetPosition)
        {
            door.transform.position = Vector3.Lerp(door.transform.position, targetPosition, time);
            time += Time.deltaTime * doorSpeed;

        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bostero")
        {
            targetPosition = openTransform.position;
            time = 0;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Bostero")
        {
            targetPosition = closedTransform.position;
            time = 0;

        }
    }
}
