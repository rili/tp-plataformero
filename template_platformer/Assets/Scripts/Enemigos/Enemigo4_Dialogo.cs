using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Enemigo4_Dialogo : MonoBehaviour
{

    [SerializeField] private GameObject marca;
    [SerializeField] public GameObject dialogopanel;
    [SerializeField] public TMP_Text dialogoTexto;
    [SerializeField, TextArea(4, 6)] private string[] dialogolinea;

    public float Tempo = 0.05f;

    static private bool IsPlayerInRange;
    public bool didDialogueStart;
    private int lineindex;

    void Start()
    {

    }

     void FixedUpdate()
    {
        GetInput();
    }


    private void GetInput()
    {
        if (GameManager.actualPlayer == 2)
        {
            if (IsPlayerInRange && Input.GetKeyDown(KeyCode.Mouse0))
            {
                if (!didDialogueStart)
                {
                    StartDialogue();
                }
                else if (dialogoTexto.text == dialogolinea[lineindex])
                {
                    NextLine();
                }
                else
                {
                    StopAllCoroutines();
                    dialogoTexto.text = dialogolinea[lineindex];
                }
            }
        }

    }

    public void StartDialogue()
    {
        didDialogueStart = true;
        dialogopanel.SetActive(true);
        marca.SetActive(false);
        lineindex = 0;
        Enemigo1_Lateral.velocidad = 0f;
        //Time.timeScale = 0f;
        StartCoroutine(ShowLine());

    }

    private IEnumerator ShowLine()
    {
        dialogoTexto.text = string.Empty;
        foreach (char ch in dialogolinea[lineindex])
        {
            dialogoTexto.text += ch;
            yield return new WaitForSecondsRealtime(Tempo);
        }
    }

    private void NextLine()
    {
        lineindex++;
        if (lineindex < dialogolinea.Length)
        {
            StartCoroutine(ShowLine());

        }
        else
        {
            didDialogueStart = false;
            dialogopanel.SetActive(false);
            marca.SetActive(true);
            Enemigo1_Lateral.velocidad = 2f;
            //Time.timeScale = 1f;
        }
    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player9"))
        {
            IsPlayerInRange = true;
            Debug.Log("todo bien");
            marca.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player9"))
        {
            IsPlayerInRange = false;
            Debug.Log("todo mal");
            marca.SetActive(false);
        }
    }
}
