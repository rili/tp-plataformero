using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo3 : MonoBehaviour
{
    public float radius = 1f;
    public float speed = 2f;

    private Vector3 center;

    private float angle = 0f;

    private void Start()
    {
        center = transform.position;
    }

    private void Update()
    {
        angle += Time.deltaTime * speed;

        float x = Mathf.Cos(angle) * radius;
        float y = Mathf.Sin(angle) * radius;

        transform.position = center + new Vector3(x, y, 0f);
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala"))
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bala"))
        {
            Destroy(this.gameObject);
        }
    }

}
