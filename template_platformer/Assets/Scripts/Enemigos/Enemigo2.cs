using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo2 : MonoBehaviour
{
    public float amplitude = 1f;
    public float frequency = 1f;
    public float speed = 2f;

    private Vector3 initialPosition;

    private void Start()
    {
        initialPosition = transform.position;
    }

    private void Update()
    {
        float y = initialPosition.y + Mathf.Sin(Time.time * frequency) * amplitude;
        //float y = initialPosition.y + Time.time * speed;

        transform.position = new (y, 0f);
    }
}