using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotador : MonoBehaviour
{
    // Start is called before the first frame update
    void Update()
    {
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player9"))
        {
            Destroy(this.gameObject);
            GameManager.winCondition = true;
        }
    }


    private void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.CompareTag("Player9"))
        {
            Destroy(this.gameObject);
            GameManager.winCondition = true;
        }
    }
}

