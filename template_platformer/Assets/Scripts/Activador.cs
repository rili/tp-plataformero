using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activador : MonoBehaviour
{
    public Transform Objeto;
    public Transform openTransform;
    public Transform closedTransform;
    public float Speed = 1f;
    Vector3 targetPosition;
    float time;


    // Start is called before the first frame update
    void Start()
    {
        targetPosition = closedTransform.position;

    }

    // Update is called once per frame
    void Update()
    {
        if (Objeto.position != targetPosition)
        {
            Objeto.transform.position = Vector3.Lerp(Objeto.transform.position, targetPosition, time);
            time += Time.deltaTime * Speed;

        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            targetPosition = openTransform.position;
            time = 0;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            targetPosition = closedTransform.position;
            time = 0;

        }
    }
}
